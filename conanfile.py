from conans import ConanFile, CMake, tools
import os

class YamlcppConan(ConanFile):
    name = "yaml-cpp"
    version = "0.6.2"
    license = "MIT"
    url = "https://github.com/jbeder/yaml-cpp"
    description = "Conan build system"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    exports_sources = "CMakeLists.txt"
    generators = "cmake"

    def source(self):
        self.run("git clone https://github.com/jbeder/yaml-cpp")
        os.rename("yaml-cpp", "sources")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["YAML_CPP_BUILD_CONTRIB"] = True
        cmake.definitions["YAML_CPP_BUILD_TOOLS"] = False
        if self.settings.os == "Windows" and self.options.shared:
            cmake.definitions["BUILD_SHARED_LIBS"] = True
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy(pattern="LICENSE", dst=".", src="sources")
        self.copy(pattern="*.h", dst="include", src=os.path.join("sources", "include"))
        self.copy(pattern="lib%s.a" % self.name, dst="lib", src="lib", keep_path=False)
        self.copy(pattern="lib%s.so*" % self.name, dst="lib", src="lib", keep_path=False)
        self.copy(pattern="lib%s*.dylib" % self.name, dst="lib", src="lib", keep_path=False)
        self.copy(pattern="*%s*.lib" % self.name, dst="lib", src="lib", keep_path=False)
        self.copy(pattern="%s.dll" % self.name, dst="bin", src="bin", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)